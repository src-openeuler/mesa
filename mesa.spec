%global llvm_toolset %{nil}
%global llvm_pkg_prefix %{nil}

# avoid opencl-related features (i.e. iris) as some dep pkgs not yet available
# in openEuler
%undefine with_opencl
%undefine with_intel_clc
%undefine with_intel_vk_rt

%ifarch s390x
%define with_hardware 0
%else
%define with_hardware 1
%define with_va 1
%define with_vdpau 1
%define with_nine 1
%endif

%ifarch %{ix86} x86_64
%define platform_drivers i965
%define with_vmware 1
%define with_xa     1
%if 0%{?with_opencl} || 0%{?with_intel_clc}
%define with_iris   1
%endif
%define with_crocus 1
%endif

%ifarch %{ix86} x86_64
%define with_vulkan_hw 1
%else
%define with_vulkan_hw 0
%endif

%ifarch %{arm} aarch64 sw_64
%define with_xa        1
%endif

%ifarch %{arm} aarch64
%define with_vc4       1
%define with_v3d       1
%define with_kmsro     1
%endif

%ifarch riscv64
%define with_xa        1
%define with_vmware    1
%endif

%global dri_drivers %{?platform_drivers}

%if 0%{?with_vulkan_hw} && (0%{?with_opencl} || 0%{?with_intel_clc})
%define vulkan_drivers swrast,virtio,intel,amd,intel_hasvk
%else
%define vulkan_drivers swrast,virtio,amd
%endif

%global sanitize 0

# We've gotten a report that enabling LTO for mesa breaks some games. See
# https://bugzilla.redhat.com/show_bug.cgi?id=1862771 for details.
# Disable LTO for now
%define _lto_cflags %{nil}

Name:           mesa
Summary:        Mesa graphics libraries
Version:        24.3.1
Release:        1

License:        MIT
URL:            https://www.mesa3d.org
Source0:        https://archive.mesa3d.org/%{name}-%{version}.tar.xz

# https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/31108/
# adding for resolving libepoxy's testing issues on x86_64 & aarch64
Patch1:        31108.patch

BuildRequires:  gcc
BuildRequires:  gcc-c++

BuildRequires:  meson >= 1.0.0
%if %{with_hardware}
BuildRequires:  kernel-headers
%endif
BuildRequires:  libdrm-devel >= 2.4.121
BuildRequires:  libXxf86vm-devel
BuildRequires:  expat-devel
BuildRequires:  xorg-x11-proto-devel
BuildRequires:  libXext-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libXdamage-devel
BuildRequires:  libXi-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libXmu-devel
BuildRequires:  libxshmfence-devel
BuildRequires:  elfutils
BuildRequires:  python3-devel
BuildRequires:  gettext
BuildRequires: %{llvm_pkg_prefix}llvm-devel >= 3.4-7
%if 0%{?with_opencl} || 0%{?with_intel_clc}
BuildRequires: %{llvm_pkg_prefix}clang-devel >= 3.0
BuildRequires: opencl-filesystem
BuildRequires: libclc-devel
BuildRequires: spirv-tools-devel
BuildRequires: spirv-llvm-translator-devel
%endif
BuildRequires: elfutils-libelf-devel
BuildRequires: /usr/bin/eu-findtextrel
BuildRequires: libudev-devel
BuildRequires: bison flex
BuildRequires: pkgconfig(wayland-client)
BuildRequires: pkgconfig(wayland-server)
BuildRequires: pkgconfig(wayland-protocols) >= 1.34
%if 0%{?with_vdpau}
BuildRequires: libvdpau-devel
%endif
%if 0%{?with_va}
BuildRequires: libva-devel
%endif
BuildRequires: pkgconfig(zlib)
BuildRequires: python3-mako
%ifarch %{valgrind_arches}
BuildRequires: pkgconfig(valgrind)
%endif
BuildRequires: pkgconfig(libglvnd) >= 1.2.0
%if 0%{?with_intel_clc}
BuildRequires: python3-ply
%endif
BuildRequires: glslang
BuildRequires: cmake
BuildRequires: python3-pyyaml

%description
%{summary}.

%package filesystem
Summary:        Mesa driver filesystem
Provides:       mesa-dri-filesystem = %{?epoch:%{epoch}:}%{version}-%{release}
Obsoletes:      mesa-dri-filesystem < %{?epoch:%{epoch}:}%{version}-%{release}
Obsoletes:      mesa-omx-drivers < %{?epoch:%{epoch}:}%{version}-%{release}

%description filesystem
%{summary}.

%package libGL
Summary:        Mesa libGL runtime libraries
Requires:       %{name}-libglapi%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:       libglvnd-glx%{?_isa} >= 1:1.2.0-1
 
%description libGL
%{summary}.

%package libGL-devel
Summary:        Mesa libGL development package
Requires:       %{name}-libGL%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:       libglvnd-devel%{?_isa} >= 1:1.2.0-1
Provides:       libGL-devel
Provides:       libGL-devel%{?_isa}

%description libGL-devel
%{summary}.

%package libEGL
Summary:        Mesa libEGL runtime libraries
Requires:       libglvnd-egl%{?_isa} >= 1:1.2.0-1

%description libEGL
%{summary}.

%package libEGL-devel
Summary:        Mesa libEGL development package
Requires:       %{name}-libEGL%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:       libglvnd-devel%{?_isa} >= 1:1.2.0-1
Provides:       libEGL-devel
Provides:       libEGL-devel%{?_isa}

%description libEGL-devel
%{summary}.

%package dri-drivers
Summary:        Mesa-based DRI drivers
Requires:       %{name}-filesystem%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:	libdrm >= 2.4.103

%description dri-drivers
%{summary}.

%if 0%{?with_va}
%package        va-drivers
Summary:        Mesa-based VA-API video acceleration drivers
Requires:       %{name}-filesystem%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description va-drivers
%{summary}.
%endif

%if 0%{?with_vdpau}
%package        vdpau-drivers
Summary:        Mesa-based VDPAU drivers
Requires:       %{name}-filesystem%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description vdpau-drivers
%{summary}.
%endif

%package libOSMesa
Summary:        Mesa offscreen rendering libraries
Requires:       %{name}-libglapi%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Provides:       libOSMesa
Provides:       libOSMesa%{?_isa}

%description libOSMesa
%{summary}.

%package libOSMesa-devel
Summary:        Mesa offscreen rendering development package
Requires:       %{name}-libOSMesa%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description libOSMesa-devel
%{summary}.

%package libgbm
Summary:        Mesa gbm runtime library
Provides:       libgbm
Provides:       libgbm%{?_isa}

%description libgbm
%{summary}.

%package libgbm-devel
Summary:        Mesa libgbm development package
Requires:       %{name}-libgbm%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Provides:       libgbm-devel
Provides:       libgbm-devel%{?_isa}

%description libgbm-devel
%{summary}.

%if 0%{?with_xa}
%package libxatracker
Summary:        Mesa XA state tracker
Provides:       libxatracker
Provides:       libxatracker%{?_isa}

%description libxatracker
%{summary}.

%package libxatracker-devel
Summary:        Mesa XA state tracker development package
Requires:       %{name}-libxatracker%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Provides:       libxatracker-devel
Provides:       libxatracker-devel%{?_isa}

%description libxatracker-devel
%{summary}.
%endif

%package libglapi
Summary:        Mesa shared glapi
Provides:       libglapi
Provides:       libglapi%{?_isa}

%description libglapi
%{summary}.

%if 0%{?with_opencl}
%package libOpenCL
Summary:        Mesa OpenCL runtime library
Requires:       ocl-icd%{?_isa}
Requires:       libclc%{?_isa}
Requires:       %{name}-libgbm%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:       opencl-filesystem

%description libOpenCL
%{summary}.

%package libOpenCL-devel
Summary:        Mesa OpenCL development package
Requires:       %{name}-libOpenCL%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description libOpenCL-devel
%{summary}.
%endif

%if 0%{?with_nine}
%package libd3d
Summary:        Mesa Direct3D9 state tracker

%description libd3d
%{summary}.

%package libd3d-devel
Summary:        Mesa Direct3D9 state tracker development package
Requires:       %{name}-libd3d%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description libd3d-devel
%{summary}.
%endif

%package vulkan-drivers
Summary:        Mesa Vulkan drivers
Requires:       vulkan%{_isa}

%description vulkan-drivers
The drivers with support for the Vulkan API.

%if 0%{?with_vulkan_hw}
%package vulkan-devel
Summary:        Mesa Vulkan development files
Requires:       %{name}-vulkan-drivers%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:       vulkan-devel

%description vulkan-devel
Headers for development with the Vulkan API.
%endif

%prep
%autosetup -n %{name}-%{version} -p1

# Make sure the build uses gnu++14 as llvm 10 headers require that
sed -i -e 's/cpp_std=gnu++11/cpp_std=gnu++17/g' meson.build

%build
export ASFLAGS="--generate-missing-build-notes=yes"
%meson -Dcpp_std=gnu++17 \
  -Dandroid-libbacktrace=disabled \
  -Dlibunwind=disabled \
  -Dlmsensors=disabled \
  -Db_ndebug=true \
  -Dplatforms=x11,wayland \
  -Dosmesa=true \
%if 0%{?with_hardware}
  -Dgallium-drivers=softpipe,llvmpipe%{?with_iris:,iris},virgl,nouveau%{?with_vmware:,svga},radeonsi,r300,r600%{?with_freedreno:,freedreno}%{?with_etnaviv:,etnaviv}%{?with_tegra:,tegra}%{?with_vc4:,vc4}%{?with_v3d:,v3d}%{?with_crocus:,crocus}%{?with_vulkan_hw:,zink} \
%else
  -Dgallium-drivers=softpipe,llvmpipe,virgl \
%endif
  -Dgallium-vdpau=%{?with_vdpau:enabled}%{!?with_vdpau:disabled} \
  -Dgallium-va=%{?with_va:enabled}%{!?with_va:disabled} \
  -Dgallium-xa=%{?with_xa:enabled}%{!?with_xa:disabled} \
  -Dgallium-nine=%{?with_nine:true}%{!?with_nine:false} \
  -Dgallium-opencl=%{?with_opencl:icd}%{!?with_opencl:disabled} \
  -Dvulkan-drivers=%{?vulkan_drivers} \
  -Dvulkan-layers=device-select \
  -Dshared-glapi=enabled \
  -Dgles1=disabled \
  -Dgles2=enabled \
  -Dopengl=true \
  -Dgbm=enabled \
  -Dvideo-codecs=all_free \
  -Dglx=dri \
  -Degl=enabled \
  -Dglvnd=enabled \
  -Dintel-rt=%{?with_intel_vk_rt:enabled}%{!?with_intel_vk_rt:disabled} \
  -Dllvm-orcjit=true \
  -Dmicrosoft-clc=disabled \
  -Dllvm=enabled \
  -Dshared-llvm=enabled \
  -Dvalgrind=%{?with_valgrind:enabled}%{!?with_valgrind:disabled} \
  -Dbuild-tests=false \
  %{nil}
%meson_build

%check
%meson_test

%install
%meson_install

# libvdpau opens the versioned name, don't bother including the unversioned
rm -vf %{buildroot}%{_libdir}/vdpau/*.so
# likewise glvnd
rm -vf %{buildroot}%{_libdir}/libGLX_mesa.so
rm -vf %{buildroot}%{_libdir}/libEGL_mesa.so
# XXX can we just not build this
rm -vf %{buildroot}%{_libdir}/libGLES*

# glvnd needs a default provider for indirect rendering where it cannot
# determine the vendor
ln -s %{_libdir}/libGLX_mesa.so.0 %{buildroot}%{_libdir}/libGLX_system.so.0

# strip out useless headers
rm -f %{buildroot}%{_includedir}/GL/w*.h

# these are shipped already in vulkan-devel
rm -f %{buildroot}/%{_includedir}/vulkan/vk_platform.h
rm -f %{buildroot}/%{_includedir}/vulkan/vulkan.h

# this keeps breaking, check it early.  note that the exit from eu-ftr is odd.
pushd %{buildroot}%{_libdir}
for i in libOSMesa*.so libGL.so ; do
    eu-findtextrel $i && exit 1
done

%files filesystem
%dir %{_libdir}/dri
%if %{with_hardware}
%if 0%{?with_vdpau}
%dir %{_libdir}/vdpau
%endif
%endif

%files libGL
%{_libdir}/libGLX_mesa.so.0*
%{_libdir}/libGLX_system.so.0*
%files libGL-devel
%dir %{_includedir}/GL/internal
%{_includedir}/GL/internal/dri_interface.h
%{_libdir}/pkgconfig/dri.pc
%{_libdir}/libglapi.so

%files libEGL
%{_datadir}/glvnd/egl_vendor.d/50_mesa.json
%{_libdir}/libEGL_mesa.so.0*
%files libEGL-devel
%dir %{_includedir}/EGL
%{_includedir}/EGL/eglmesaext.h
%{_includedir}/EGL/eglext_angle.h

%files libglapi
%{_libdir}/libglapi.so.0
%{_libdir}/libglapi.so.0.*

%files libOSMesa
%{_libdir}/libOSMesa.so.8*
%files libOSMesa-devel
%dir %{_includedir}/GL
%{_includedir}/GL/osmesa.h
%{_libdir}/libOSMesa.so
%{_libdir}/pkgconfig/osmesa.pc

%files libgbm
%{_libdir}/libgbm.so.1
%{_libdir}/libgbm.so.1.*
%{_libdir}/gbm/dri_gbm.so

%files libgbm-devel
%{_libdir}/libgbm.so
%{_includedir}/gbm.h
%{_libdir}/pkgconfig/gbm.pc

%if 0%{?with_xa}
%files libxatracker
%if %{with_hardware}
%{_libdir}/libxatracker.so.2
%{_libdir}/libxatracker.so.2.*
%endif

%files libxatracker-devel
%if %{with_hardware}
%{_libdir}/libxatracker.so
%{_includedir}/xa_tracker.h
%{_includedir}/xa_composite.h
%{_includedir}/xa_context.h
%{_libdir}/pkgconfig/xatracker.pc
%endif
%endif

%if 0%{?with_opencl}
%files libOpenCL
%{_libdir}/libMesaOpenCL.so.*
%{_sysconfdir}/OpenCL/vendors/mesa.icd
%files libOpenCL-devel
%{_libdir}/libMesaOpenCL.so
%endif

%if 0%{?with_nine}
%files libd3d
%dir %{_libdir}/d3d/
%{_libdir}/d3d/*.so.*
 
%files libd3d-devel
%{_libdir}/pkgconfig/d3d.pc
%{_includedir}/d3dadapter/
%{_libdir}/d3d/*.so
%endif

%files dri-drivers
%dir %{_datadir}/drirc.d
%{_datadir}/drirc.d/00-mesa-defaults.conf
%{_libdir}/libgallium-*.so
%if %{with_hardware}
%{_libdir}/dri/r300_dri.so
%{_libdir}/dri/r600_dri.so
%{_libdir}/dri/radeonsi_dri.so
%ifarch %{ix86} x86_64
%{_libdir}/dri/crocus_dri.so
%if 0%{?with_opencl} || 0%{?with_intel_clc}
%{_libdir}/dri/iris_dri.so
%endif
%endif
%if 0%{?with_vc4}
%{_libdir}/dri/vc4_dri.so
%endif
%if 0%{?with_v3d}
%{_libdir}/dri/v3d_dri.so
%endif
%if 0%{?with_kmsro}
%{_libdir}/dri/armada-drm_dri.so
%{_libdir}/dri/exynos_dri.so
%{_libdir}/dri/hdlcd_dri.so
%{_libdir}/dri/hx8357d_dri.so
%{_libdir}/dri/ili9225_dri.so
%{_libdir}/dri/ili9341_dri.so
%{_libdir}/dri/imx-drm_dri.so
%{_libdir}/dri/imx-dcss_dri.so
%{_libdir}/dri/imx-lcdif_dri.so
%{_libdir}/dri/ingenic-drm_dri.so
%{_libdir}/dri/kirin_dri.so
%{_libdir}/dri/komeda_dri.so
%{_libdir}/dri/mali-dp_dri.so
%{_libdir}/dri/mcde_dri.so
%{_libdir}/dri/mediatek_dri.so
%{_libdir}/dri/meson_dri.so
%{_libdir}/dri/mi0283qt_dri.so
%{_libdir}/dri/mxsfb-drm_dri.so
%{_libdir}/dri/pl111_dri.so
%{_libdir}/dri/rcar-du_dri.so
%{_libdir}/dri/repaper_dri.so
%{_libdir}/dri/rockchip_dri.so
%{_libdir}/dri/rzg2l-du_dri.so
%{_libdir}/dri/ssd130x_dri.so
%{_libdir}/dri/st7586_dri.so
%{_libdir}/dri/st7735r_dri.so
%{_libdir}/dri/stm_dri.so
%{_libdir}/dri/sun4i-drm_dri.so
%{_libdir}/dri/vkms_dri.so
%{_libdir}/dri/zynqmp-dpsub_dri.so
%endif
%if 0%{?with_freedreno}
%{_libdir}/dri/kgsl_dri.so
%{_libdir}/dri/msm_dri.so
%endif
%if 0%{?with_etnaviv}
%{_libdir}/dri/etnaviv_dri.so
%{_libdir}/dri/imx-drm_dri.so
%endif
%{_libdir}/dri/nouveau_dri.so
%if 0%{?with_vmware}
%{_libdir}/dri/vmwgfx_dri.so
%endif
%endif
%{_libdir}/dri/kms_swrast_dri.so
%{_libdir}/dri/libdril_dri.so
%{_libdir}/dri/swrast_dri.so
%{_libdir}/dri/virtio_gpu_dri.so
%ifarch %{arm} aarch64
%{_libdir}/dri/gm12u320_dri.so
%{_libdir}/dri/ili9163_dri.so
%{_libdir}/dri/ili9486_dri.so
%{_libdir}/dri/panel-mipi-dbi_dri.so
%{_libdir}/dri/sti_dri.so
%{_libdir}/dri/udl_dri.so
%endif

%ifarch %{ix86} x86_64 %{arm} aarch64 riscv64
%{_libdir}/dri/zink_dri.so
%endif

%if %{with_hardware}
%if 0%{?with_va}
%files va-drivers
%{_libdir}/dri/nouveau_drv_video.so
%{_libdir}/dri/r600_drv_video.so
%{_libdir}/dri/radeonsi_drv_video.so
%{_libdir}/dri/virtio_gpu_drv_video.so
%endif
%if 0%{?with_vdpau}
%files vdpau-drivers
%{_libdir}/vdpau/libvdpau_nouveau.so.1*
%{_libdir}/vdpau/libvdpau_r600.so.1*
%{_libdir}/vdpau/libvdpau_radeonsi.so.1*
%{_libdir}/vdpau/libvdpau_virtio_gpu.so.1*
%endif
%endif

%files vulkan-drivers
%if 0%{?with_vulkan_hw} && (0%{?with_opencl} || 0%{?with_intel_clc})
%{_libdir}/libvulkan_intel.so
%{_libdir}/libvulkan_intel_hasvk.so
%{_datadir}/vulkan/icd.d/intel_hasvk_icd.*.json
%{_datadir}/vulkan/icd.d/intel_icd.*.json
%endif
%{_datadir}/drirc.d/00-radv-defaults.conf
%{_datadir}/vulkan/icd.d/radeon_icd.*.json
%{_libdir}/vdpau/libvdpau_virtio_gpu.so.1*
%{_libdir}/libvulkan_radeon.so
%{_libdir}/libvulkan_lvp.so
%{_datadir}/vulkan/icd.d/lvp_icd.*.json
%{_libdir}/libvulkan_virtio.so
%{_datadir}/vulkan/icd.d/virtio_icd.*.json
%{_libdir}/libVkLayer_MESA_device_select.so
%{_datadir}/vulkan/implicit_layer.d/VkLayer_MESA_device_select.json

%if 0%{?with_vulkan_hw}
%files vulkan-devel
%endif

%changelog
* Thu Dec 05 2024 Funda Wang <fundawang@yeah.net> - 24.3.1-1
- update to 24.3.1

* Sun Nov 24 2024 Funda Wang <fundawang@yeah.net> - 24.3.0-1
- update to 24.3.0
- disable LTO build
- omx support dropped upstream

* Tue Sep 24 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 24.2.1-3
- fix the `unpackaged file zink_dri.so issue` for riscv64

* Tue Sep 10 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 24.2.1-2
- add upstream gallivm patch, enable zink for {x86_64,aarch64}

* Fri Aug 16 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 24.2.1-1
- update to version 24.2.1
- add va-drivers back, set video-codecs to 'all-free'
- drop obsolete (mainlined) llvmpipe/orcjit patches for loong64/riscv64
- disable opencl-related features as deps missing

* Mon May 20 2024 zhaojiale <zhaojiale@loongson.cn> - 24.0.3-2
- add upstream orcjit patch and support loongarch64 orcjit

* Mon Mar 18 2024 liweigang <liweiganga@uniontech.com> - 24.0.3-1
- update to version 24.0.3

* Tue Dec 19 2023 zhangpan <zhangpan103@h-partners.com> - 23.3.1-1
- upgrade to mesa-23.3.1

* Thu Nov 02 2023 Jingwiw  <wangjingwei@iscas.ac.cn> - 23.2.1-1
- upgrade to version 23.2.1
- fix llvmpipe interface support for the new version

* Sun Aug 20 2023 Funda Wang <fundawang@yeah.net> - 23.1.6-1
- update to 23.1.6

* Fri Aug 11 2023 xiaofan <xiaofan@iscas.ac.cn> - 23.1.3-3
- Enable gallium driver vc4/v3d for arm platform

* Tue Aug 08 2023 Jingwiw  <wangjingwei@iscas.ac.cn> - 23.1.3-2
- Add OrcJIT and add riscv architecture optimization
- Optimize the mesa spec and add more backend

* Mon Jul 31 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 23.1.3-1
- upgrade to mesa-23.1.3

* Thu Nov 3 2022 wuzx<wuzx1226@qq.com> - 21.3.1-3
- Add sw64 architecture

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 21.3.1-2
- Rebuild for next release

* Thu Sep 16 2021 hanhui <hanhui15@huawei.com> - 21.3.1-1
- upgrade to mesa-21.3.1
- enable check

* Thu Mar 25 2021 yanan <yanan@huawei.com> - 20.1.4-2
- optimize the mesa spec

* Sat Oct 10 2020 hanhui <hanhui15@huawei.com> - 20.1.4-1
- update to 20.1.4

* Wed Jun 03 2020 songnannan <songnannan2@huawei.com> - 18.3.6-2
- add mesa-khr-header subpackage to hold <KHR/khrplatform.h>

* Tue Jun 02 2020 songnannan <songnannan2@huawei.com> - 18.3.6-1
- update to 18.3.6

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 18.2.2-6
- disable opencl

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 18.2.2-5
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the license file

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 18.2.2-4
- Type: enhance
- Id:NA
- SUG:NA
- DESC: rewrite it without merging packages

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 18.2.2-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC: restore previous version of 18.2.2-1

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 18.2.2-2
- Package init
